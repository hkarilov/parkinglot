package org.hanada.repository;

import org.hanada.entity.Spot;
import org.hanada.entity.Vehicle;
import org.hanada.entity.VehicleSpot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hkarilov on 07 March, 2021
 */
@Repository
public interface VehicleSpotRepository extends JpaRepository<VehicleSpot, Integer> {

    VehicleSpot findTop1ByVehicleAndSpotOrderByIdDesc(Vehicle vehicle, Spot spot);

}
