package org.hanada.service;

import lombok.RequiredArgsConstructor;
import org.hanada.entity.Spot;
import org.hanada.entity.Vehicle;
import org.hanada.entity.VehicleSpot;
import org.hanada.model.VehicleSpotModel;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by hkarilov on 07 March, 2021
 */
@RequiredArgsConstructor
@Service
public class VehicleSpotServiceEndpoint {

    private final SpotService spotService;

    private final VehicleService vehicleService;

    private final VehicleSpotService vehicleSpotService;

    public boolean hasAvailableSpot(){
        return spotService.hasAvailableSpot();
    }

    public boolean  updateVehicleSpotStatus(VehicleSpotModel vehicleSpotModel){
        Spot spot = spotService.getById(vehicleSpotModel.getSpotVos());

        if (vehicleSpotModel.getBlock())
            return blockSpot(vehicleSpotModel, spot);

        return unBlockSpot(vehicleSpotModel, spot);
    }

    private boolean blockSpot(VehicleSpotModel vehicleSpotModel, Spot spot){

        if(!spot.getAvailable()){
            throw new RuntimeException("Parking spot not available");
        }

        spot.setAvailable(false);
        VehicleSpot vehicleSpot = new VehicleSpot(spot);
        Vehicle vehicle = vehicleService.getByRegistrationNo(vehicleSpotModel.getVehicleRegistrationNo());
        if(vehicle != null)
            vehicleSpot.setVehicle(vehicle);
        else
            vehicleSpot.setVehicle(vehicleService.save(new Vehicle(vehicleSpotModel.getVehicleRegistrationNo())));

        return vehicleSpotService.save(vehicleSpot) != null;
    }

    private boolean unBlockSpot(VehicleSpotModel vehicleSpotModel, Spot spot){
        if(spot.getAvailable())
            throw new RuntimeException("Parking spot available");

        Vehicle vehicle = vehicleService.getByRegistrationNo(vehicleSpotModel.getVehicleRegistrationNo());

        VehicleSpot vehicleSpot = vehicleSpotService.getVehicleSpotByVehicleAndSpot(vehicle, spot);

        if(vehicleSpot.getOutTime() != null)
            throw new RuntimeException("Vehicle not in the parking");
        spot.setAvailable(true);
        vehicleSpot.setSpot(spot);
        vehicleSpot.setOutTime(LocalDateTime.now());
        return vehicleSpotService.save(vehicleSpot) != null;
    }

}
