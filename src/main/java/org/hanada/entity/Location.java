package org.hanada.entity;

import lombok.*;
import org.hanada.entity.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by hkarilov on 07 March, 2021
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "parking_location")
public class Location extends BaseEntity {

    private String name;

    private Integer level;

}
