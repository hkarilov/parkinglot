package org.hanada.controller.parking;

import lombok.RequiredArgsConstructor;
import org.hanada.model.VehicleSpotModel;
import org.hanada.service.VehicleSpotServiceEndpoint;
import org.springframework.web.bind.annotation.*;

/**
 * Created by hkarilov on 07 March, 2021
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/parking")
public class ParkingController {

    private final VehicleSpotServiceEndpoint serviceEndpoint;

    /**
     * Check has parking any available spot
     * @return true - if has, false - if not
     */
    @GetMapping("/check-parking")
    public boolean checkParking(){
        return serviceEndpoint.hasAvailableSpot();
    }

    @PostMapping("/update-status")
    public boolean updateSpotStatus(@RequestBody VehicleSpotModel vehicleSpotModel){
        return serviceEndpoint.updateVehicleSpotStatus(vehicleSpotModel);
    }

}
