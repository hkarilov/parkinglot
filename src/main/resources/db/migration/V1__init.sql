CREATE TABLE parking_location
(
  id                SERIAL,
  name              VARCHAR(50) NULL UNIQUE,
  level             INTEGER,
  created_at        TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id)
);