package org.hanada;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.hanada.entity.Location;
import org.hanada.entity.Spot;
import org.hanada.helpers.Builder;
import org.hanada.model.VehicleSpotModel;
import org.hanada.service.SpotService;
import org.hanada.service.VehicleSpotServiceEndpoint;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ParkingLotControllersIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VehicleSpotServiceEndpoint serviceEndpoint;

    @MockBean
    private SpotService service;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void test1Sensor() throws Exception {


        VehicleSpotModel busy = new VehicleSpotModel("08KG110AAA", 6, Boolean.TRUE);
        Mockito.when(serviceEndpoint.updateVehicleSpotStatus(busy)).thenReturn(true);
		this.mockMvc.perform(
				post("/api/parking/update-status")
						.content(objectMapper.writeValueAsString(busy))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
		)
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("true"));

        VehicleSpotModel free = new VehicleSpotModel("08KG110AAA", 6, Boolean.FALSE);
        Mockito.when(serviceEndpoint.updateVehicleSpotStatus(free)).thenReturn(true);
		this.mockMvc.perform(
                post("/api/parking/update-status")
						.content(objectMapper.writeValueAsString(free))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
		)
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("true"));
    }

    @Test
    public void test2InGate() throws Exception {
        Mockito.when(serviceEndpoint.hasAvailableSpot()).thenReturn(true);
        this.mockMvc.perform(
                get("/api/parking/check-parking/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    public void test3Dashboard() throws Exception {
        Mockito.when(service.getAvailableSpots()).thenReturn(initAvailableSpot());
        this.mockMvc.perform(
                get("/api/dashboard/get-available-spots/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(initAvailableSpot())));
    }

    private List<Spot> initAvailableSpot(){
        return Arrays.asList(Builder.build(Spot.class)
                .with(r -> r.setId(6))
                .with(r -> r.setPos(2))
                .with(r -> r.setAvailable(true))
                .with(r -> r.setLocation(Builder.build(Location.class).with( l -> l.setId(1))
                        .with(l -> l.setLevel(2))
                        .with(l -> l.setName("Section B1"))
                        .get()))
                .get());
    }

}
