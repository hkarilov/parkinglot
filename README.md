# README #

T### Requirements before running application:
 
 * Java 8
 * Gradle
 

 ### Run service
 Service runs on 9292 port
 
 Run service from CMD: java -Xms128m -Xmx512m -jar parking-lot.jar

 
 ### Dev profile environment variables
 DB_HOST=Your local db host
 
 DB_PORT=Your local db port;
 
 DB_NAME=Your local db name;
 
 DB_USER=Your local db user;
 
 DB_PASS=Your local db password!;
 
 ### API
 
 GET /api/parking/check-parking - Проверка свободных мест true - есть, false - нет
 
 
 GET /api/dashboard/get-available-spots - Получить список совободных мест success json list of model [ {   
   "id": 6,   
   "pos": 2,  
    "available": true,   
    "location": {   
      "id": 3,   
      "name": "Section B1",   
      "level": 2 }   
    } ]
 
 
 GET /api/dashboard/get-all-spots - Получить весь список парковочных мест success json list of spot model   
 [ {   
   "id": 6,   
   "pos": 2,  
    "available": true, // available true - парковочное место свободно, false - занято   
    "location": {   
      "id": 3,   
      "name": "Section B1",  
      "level": 2 }   
    } ]
  
 
 
 POST /api/parking/update-status - блокировка места при вьезде и разблокировка при выезде body json model
 
 {  
 
     "vehicleRegistrationNo":"08KG110AAA", // регистрационный номер машины
     "spotVos": 6, // id места парковки (pot model id)
     "block" : false // true -при блокировке места, false - при разблокировки места
     
 }
 
 response - true при успешной операции