package org.hanada.repository;

import org.hanada.entity.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by hkarilov on 07 March, 2021
 */
@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {
    
    Vehicle findByRegistrationNo(String registrationNo);
    
}
