package org.hanada.service;

import lombok.RequiredArgsConstructor;
import org.hanada.entity.Vehicle;
import org.hanada.repository.VehicleRepository;
import org.springframework.stereotype.Service;

/**
 * Created by hkarilov on 07 March, 2021
 */
@RequiredArgsConstructor
@Service
public class VehicleService {
    
    private final VehicleRepository repository;
    
    public Vehicle getByRegistrationNo(String registrationNo){
        Vehicle vehicle = repository.findByRegistrationNo(registrationNo);
        if(vehicle == null)
            throw new RuntimeException("Vehicle not found");
        return vehicle;
    }

    public Vehicle save(Vehicle vehicle){
        return repository.save(vehicle);
    }
}
