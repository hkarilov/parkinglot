package org.hanada.controller.dashboard;

import lombok.RequiredArgsConstructor;
import org.hanada.entity.Spot;
import org.hanada.service.SpotService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by hkarilov on 07 March, 2021
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/api/dashboard")
public class SpotController {

    private final SpotService service;

    /**
     * @return only available spot list
     */
    @GetMapping("/get-available-spots")
    public List<Spot> getAvailableSpots(){
        return service.getAvailableSpots();
    }

    /**
     * @return all spot list
     */
    @GetMapping("/get-all-spots")
    public List<Spot> getAllSpots(){
        return service.getAll();
    }
}
