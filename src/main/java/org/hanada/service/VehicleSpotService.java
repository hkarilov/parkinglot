package org.hanada.service;

import lombok.RequiredArgsConstructor;

import org.hanada.entity.Spot;
import org.hanada.entity.Vehicle;
import org.hanada.entity.VehicleSpot;
import org.hanada.repository.VehicleSpotRepository;
import org.springframework.stereotype.Service;


/**
 * Created by hkarilov on 07 March, 2021
 */
@RequiredArgsConstructor
@Service
public class VehicleSpotService {

    private final VehicleSpotRepository repository;

    public VehicleSpot getVehicleSpotByVehicleAndSpot(Vehicle vehicle, Spot spot){
        VehicleSpot vehicleSpot = repository.findTop1ByVehicleAndSpotOrderByIdDesc(vehicle, spot);
        if(vehicleSpot == null)
            throw new RuntimeException("Vehicle spot  not found");
        return vehicleSpot;
    }

    public VehicleSpot save(VehicleSpot vehicleSpot){
        return repository.save(vehicleSpot);
    }


}
