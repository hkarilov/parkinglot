package org.hanada.entity;

import lombok.*;
import org.hanada.entity.base.BaseEntity;

import javax.persistence.*;

/**
 * Created by hkarilov on 07 March, 2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "parking_spot")
public class Spot extends BaseEntity {

    private Integer pos;

    private Boolean available = Boolean.TRUE;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;

}
