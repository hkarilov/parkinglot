package org.hanada.helpers;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;

@Slf4j
public class Builder<T> {

  private T instance;
  private boolean ifCond = true;

  private Builder(Class<T> clazz) {
    try {
      instance = clazz.newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      log.error("Error inside builder constructor: ", e);
    }
  }

  public static <T> Builder<T> build(Class<T> clazz) {
    return new Builder<>(clazz);
  }

  public Builder<T> with(Consumer<T> setter) {
    if (ifCond) {
      setter.accept(instance);
    }
    return this;
  }

  public T get() {
    return instance;
  }

  public Builder<T> endIf() {
    this.ifCond = true;
    return this;
  }
}
