package org.hanada.service;

import lombok.RequiredArgsConstructor;
import org.hanada.entity.Spot;
import org.hanada.repository.SpotRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hkarilov on 07 March, 2021
 */
@RequiredArgsConstructor
@Service
public class SpotService {

    private final SpotRepository repository;

    public List<Spot> getAvailableSpots(){
        return repository.findAllByAvailable(true);
    }

    public List<Spot> getAll(){
        return repository.findAll();
    }

    public Boolean hasAvailableSpot(){
        return repository.existsAllByAvailableIsTrue();
    }

    public Spot getById(Integer id){
        Spot spot =  repository.getOne(id);
        if(spot == null)
            throw new RuntimeException("Parking spot not found");
        return spot;
    }

    public Spot save(Spot spot){
        return repository.save(spot);
    }
}
