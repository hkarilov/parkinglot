package org.hanada.repository;

import org.hanada.entity.Spot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hkarilov on 07 March, 2021
 */
@Repository
public interface SpotRepository extends JpaRepository<Spot, Integer> {

    List<Spot> findAllByAvailable(Boolean available);

    Boolean existsAllByAvailableIsTrue();

}
