package org.hanada.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleSpotModel implements Serializable {

    @NotBlank
    private String vehicleRegistrationNo;

    @NonNull
    private Integer spotVos;

    @NonNull
    private Boolean block;

}
