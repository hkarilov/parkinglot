package org.hanada.entity;

import lombok.*;
import org.hanada.entity.base.BaseEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by hkarilov on 07 March, 2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "vehicle_spot")
public class VehicleSpot extends BaseEntity {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "spot_id")
    private Spot spot;

    private LocalDateTime inTime;

    private LocalDateTime outTime;

    public VehicleSpot(Spot spot) {
        this.spot = spot;
        this.inTime = LocalDateTime.now();
    }

}
